import React, {useEffect, useState} from 'react';
import {PokemonDetail} from './PokemonDetail';
import {getPokemonByName, getPokemonSpeciesByName} from '../API/PokeApi';

export function AcierDetail({route}) {
  const [pokemon, setPokemon] = useState([]);
  const [pokemonSpecies, setPokemonSpecies] = useState([]);
  useEffect(() => {
    getPokemonByName(route.params.nomPokemon).then(data => setPokemon(data));
    getPokemonSpeciesByName(route.params.nomPokemon).then(data =>
      setPokemonSpecies(data),
    );
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [pokemon, pokemonSpecies]);
  return <PokemonDetail pokemon={pokemon} pokemonSpecies={pokemonSpecies} />;
}
