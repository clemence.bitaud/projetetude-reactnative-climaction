import React, {useEffect, useState} from 'react';
import {View} from 'react-native';
import {PokemonList} from './PokemonList';
import {getPokemonByType} from '../API/PokeApi';

export const Acier = ({navigation}) => {
  const [pokemon, setPokemon] = useState([]);
  useEffect(() => {
    getPokemonByType('9').then(data => setPokemon(data.pokemon));
  }, [pokemon]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <PokemonList
        pokemonData={pokemon}
        navigateTo={'AcierDetail'}
        navigation={navigation}
      />
    </View>
  );
};
