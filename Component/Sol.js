import * as React from 'react';
import {Button, View, Text} from 'react-native';
import {useState} from 'react';
import {useEffect} from 'react';
import {getPokemonByType} from '../API/PokeApi';
import {PokemonList} from './PokemonList';

export function Sol({navigation}) {
  const [pokemon, setPokemon] = useState([]);
  useEffect(() => {
    getPokemonByType('5').then(data => setPokemon(data.pokemon));
  }, [pokemon]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <PokemonList
        pokemonData={pokemon}
        navigateTo={'SolDetail'}
        navigation={navigation}
      />
    </View>
  );
}
