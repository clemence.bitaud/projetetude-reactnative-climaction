import {Image, ScrollView, StyleSheet, Text, View} from 'react-native';
import * as React from 'react';

export const PokemonDetail = ({pokemon, pokemonSpecies}) => {
  return (
    <ScrollView>
      <Text>{pokemon.name}</Text>
      <View style={styles.image_container}>
        <Image
          style={styles.image}
          source={{
            uri:
              'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/other/official-artwork/' +
              pokemon.id +
              '.png',
          }}
        />
      </View>
      <Text>{pokemon.height}</Text>
      <Text>{pokemon.weight}</Text>
    </ScrollView>
  );
};

const styles = StyleSheet.create({
  main_container: {
    flex: 1,
    flexDirection: 'column',
  },
  image_container: {
    alignItems: 'center',
  },
  image: {
    width: 250,
    height: 250,
    margin: 5,
  },
  protocol_text: {
    fontSize: 14,
    textAlign: 'center',
    marginTop: 10,
    marginBottom: 10,
  },
  button_container: {
    alignItems: 'center',
    justifyContent: 'flex-end',
  },
  button: {
    width: '85%',
    height: 40,
    backgroundColor: '#937298',
    justifyContent: 'center',
    alignItems: 'center',
  },
  button_text: {
    color: '#FFFFFF',
    fontSize: 20,
  },
  textinput_container: {
    alignItems: 'center',
  },
  textinput: {
    marginLeft: 5,
    marginRight: 5,
    height: 50,
    width: '85%',
    borderColor: '#D8D5D5',
    borderWidth: 1,
    paddingLeft: 5,
    marginBottom: 10,
    backgroundColor: '#D8D5D5',
  },
});
