import {TouchableOpacity, StyleSheet, Image, View, Text} from 'react-native';
import React from 'react';

export const PokemonListItem = ({pokemon, displayPokemonDetail}) => {
  const image = urlImage => {
    let image = urlImage.split('/');
    let imagePokemon = image[6];
    return (
      'https://raw.githubusercontent.com/PokeAPI/sprites/master/sprites/pokemon/' +
      imagePokemon +
      '.png'
    );
  };

  return (
    <TouchableOpacity
      style={styles.main_container}
      onPress={() => displayPokemonDetail(pokemon.pokemon.name)}>
      <Image style={styles.image} source={{uri: image(pokemon.pokemon.url)}} />
      <View>
        <Text style={styles.name_text}>{pokemon.pokemon.name}</Text>
      </View>
    </TouchableOpacity>
  );
};

const styles = StyleSheet.create({
  main_container: {
    display: 'flex',
    height: 110,
    flexDirection: 'row',
  },
  image: {
    width: 100,
    height: 100,
    margin: 5,
  },
  content_container: {
    flex: 1,
    justifyContent: 'center',
  },
  name_text: {
    fontSize: 30,
    fontWeight: 'bold',
  },
});
