import {FlatList} from 'react-native';
import {PokemonListItem} from './PokemonListItem';
import React from 'react';

export const PokemonList = ({pokemonData, navigateTo, navigation}) => {
  const _displayPokemonDetail = nomPokemon => {
    console.log('nom : ' + nomPokemon);
    navigation.navigate(navigateTo, {nomPokemon: nomPokemon});
  };

  return (
    <FlatList
      data={pokemonData}
      keyExtractor={item => item.pokemon.name.toString()}
      renderItem={({item}) => (
        <PokemonListItem
          pokemon={item}
          displayPokemonDetail={_displayPokemonDetail}
        />
      )}
    />
  );
};
