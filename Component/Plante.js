import * as React from 'react';
import {Button, View, Text} from 'react-native';
import {getPokemonByType} from '../API/PokeApi';
import {PokemonList} from './PokemonList';
import {useState} from 'react';
import {useEffect} from 'react';

export function Plante({navigation}) {
  const [pokemon, setPokemon] = useState([]);
  useEffect(() => {
    getPokemonByType('12').then(data => setPokemon(data.pokemon));
  }, [pokemon]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <PokemonList
        pokemonData={pokemon}
        navigateTo={'PlanteDetail'}
        navigation={navigation}
      />
    </View>
  );
}
