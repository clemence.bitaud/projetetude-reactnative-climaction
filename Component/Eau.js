import * as React from 'react';
import {Button, View, Text} from 'react-native';
import {useState} from 'react';
import {useEffect} from 'react';
import {getPokemonByType} from '../API/PokeApi';
import {PokemonList} from './PokemonList';

export function Eau({navigation}) {
  const [pokemon, setPokemon] = useState([]);
  useEffect(() => {
    getPokemonByType('11').then(data => setPokemon(data.pokemon));
  }, [pokemon]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <PokemonList
        pokemonData={pokemon}
        navigateTo={'EauDetail'}
        navigation={navigation}
      />
    </View>
  );
}
