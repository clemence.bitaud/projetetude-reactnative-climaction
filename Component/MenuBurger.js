import {View, TouchableOpacity, Image} from 'react-native';
import React from 'react';

export function MenuBurger({navigation}) {
  return (
    <View style={{flex: 1, justifyContent: 'center', alignItems: 'center'}}>
      <TouchableOpacity onPress={() => navigation.openDrawer()} />
      <Image
        source={require('../assets/drawer.png')}
        style={{width: 35, height: 35, marginLeft: 10}}
      />
    </View>
  );
}
