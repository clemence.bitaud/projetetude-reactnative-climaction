import * as React from 'react';
import {View} from 'react-native';
import {getPokemonByType} from '../API/PokeApi';
import {PokemonList} from './PokemonList';
import {useState} from 'react';
import {useEffect} from 'react';

export function Insecte({navigation}) {
  const [pokemon, setPokemon] = useState([]);
  useEffect(() => {
    getPokemonByType('7').then(data => setPokemon(data.pokemon));
  }, [pokemon]);

  return (
    <View style={{flex: 1, alignItems: 'center', justifyContent: 'center'}}>
      <PokemonList
        pokemonData={pokemon}
        navigateTo={'InsecteDetail'}
        navigation={navigation}
      />
    </View>
  );
}
