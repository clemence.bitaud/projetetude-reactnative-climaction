import {Navigation} from './Navigation/Navigations';
import React, {Component} from 'react';

export default class App extends Component {
  render() {
    return <Navigation />;
  }
}
