import {FeuDetail} from '../Component/FeuDetail';
import * as React from 'react';
import {Feu} from '../Component/Feu';
import {createStackNavigator} from '@react-navigation/stack';
import {Acier} from '../Component/Acier';
import {AcierDetail} from '../Component/AcierDetail';
import {Combat} from '../Component/Combat';
import {CombatDetail} from '../Component/CombatDetail';
import {Electrik} from '../Component/Electrik';
import {ElectrikDetail} from '../Component/ElectrikDetail';
import {Dragon} from '../Component/Dragon';
import {DragonDetail} from '../Component/DragonDetail';
import {Eau} from '../Component/Eau';
import {EauDetail} from '../Component/EauDetail';
import {Fee} from '../Component/Fee';
import {FeeDetail} from '../Component/FeeDetail';
import {MenuBurger} from '../Component/MenuBurger';
import {HeaderBackButton} from '@react-navigation/stack';
import {Glace} from '../Component/Glace';
import {GlaceDetail} from '../Component/GlaceDetail';
import {Insecte} from '../Component/Insecte';
import {InsecteDetail} from '../Component/InsecteDetail';
import {Normal} from '../Component/Normal';
import {NormalDetail} from '../Component/NormalDetail';
import {Plante} from '../Component/Plante';
import {PlanteDetail} from '../Component/PlanteDetail';
import {Poison} from '../Component/Poison';
import {PoisonDetail} from '../Component/PoisonDetail';
import {Psy} from '../Component/Psy';
import {PsyDetail} from '../Component/PsyDetail';
import {Roche} from '../Component/Roche';
import {RocheDetail} from '../Component/RocheDetail';
import {Sol} from '../Component/Sol';
import {SolDetail} from '../Component/SolDetail';
import {Spectre} from '../Component/Spectre';
import {SpectreDetail} from '../Component/SpectreDetail';
import {Tenebre} from '../Component/Tenebre';
import {TenebreDetail} from '../Component/TenebreDetail';
import {Vol} from '../Component/Vol';
import {VolDetail} from '../Component/VolDetail';

const Stack = createStackNavigator();

export function AcierStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Acier"
        component={Acier}
        options={{
          headerStyle: {
            backgroundColor: '#5b8ca3',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="AcierDetail"
        component={AcierDetail}
        options={{
          headerStyle: {
            backgroundColor: '#5b8ca3',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function CombatStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Combat"
        component={Combat}
        options={{
          headerStyle: {
            backgroundColor: '#cb436b',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="CombatDetail"
        component={CombatDetail}
        options={{
          headerStyle: {
            backgroundColor: '#cb436b',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function DragonStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Dragon"
        component={Dragon}
        options={{
          headerStyle: {
            backgroundColor: '#0c6bca',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="DragonDetail"
        component={DragonDetail}
        options={{
          headerStyle: {
            backgroundColor: '#0c6bca',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function EauStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Eau"
        component={Eau}
        options={{
          headerStyle: {
            backgroundColor: '#4c93d4',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="EauDetail"
        component={EauDetail}
        options={{
          headerStyle: {
            backgroundColor: '#4c93d4',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function ElectrikStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Electrik"
        component={Electrik}
        options={{
          headerStyle: {
            backgroundColor: '#f4d33c',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="ElectrikDetail"
        component={ElectrikDetail}
        options={{
          headerStyle: {
            backgroundColor: '#f4d33c',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function FeeStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Fée"
        component={Fee}
        options={{
          headerStyle: {
            backgroundColor: '#ec8ce4',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="FéeDetail"
        component={FeeDetail}
        options={{
          headerStyle: {
            backgroundColor: '#ec8ce4',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function FeuStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Feu"
        component={Feu}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#fb9c54',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="FeuDetail"
        component={FeuDetail}
        options={{
          headerStyle: {
            backgroundColor: '#fb9c54',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function GlaceStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Glace"
        component={Glace}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#74cbbc',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="GlaceDetail"
        component={GlaceDetail}
        options={{
          headerStyle: {
            backgroundColor: '#74cbbc',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function InsecteStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Insecte"
        component={Insecte}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#93c32c',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="InsecteDetail"
        component={InsecteDetail}
        options={{
          headerStyle: {
            backgroundColor: '#93c32c',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function NormalStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Normal"
        component={Normal}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#939ba3',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="NormalDetail"
        component={NormalDetail}
        options={{
          headerStyle: {
            backgroundColor: '#939ba3',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function PlanteStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Plante"
        component={Plante}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#64bb5b',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="PlanteDetail"
        component={PlanteDetail}
        options={{
          headerStyle: {
            backgroundColor: '#64bb5b',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function PoisonStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Poison"
        component={Poison}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#ab6bc4',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="PoisonDetail"
        component={PoisonDetail}
        options={{
          headerStyle: {
            backgroundColor: '#ab6bc4',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function PsyStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Psy"
        component={Psy}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#fb7374',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="PsyDetail"
        component={PsyDetail}
        options={{
          headerStyle: {
            backgroundColor: '#fb7374',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function RocheStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Roche"
        component={Roche}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#c4b48b',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="RocheDetail"
        component={RocheDetail}
        options={{
          headerStyle: {
            backgroundColor: '#c4b48b',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function SolStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Sol"
        component={Sol}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#db7344',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="SolDetail"
        component={SolDetail}
        options={{
          headerStyle: {
            backgroundColor: '#db7344',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function SpectreStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Spectre"
        component={Spectre}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#5568aa',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="SpectreDetail"
        component={SpectreDetail}
        options={{
          headerStyle: {
            backgroundColor: '#5568aa',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function TenebreStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Tenebre"
        component={Tenebre}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#5b5464',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="TenebreDetail"
        component={TenebreDetail}
        options={{
          headerStyle: {
            backgroundColor: '#5b5464',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}

export function VolStack() {
  return (
    <Stack.Navigator>
      <Stack.Screen
        name="Vol"
        component={Vol}
        options={{
          // headerLeft: navigation => <MenuBurger navigation={navigation} />,
          headerStyle: {
            backgroundColor: '#8ea9df',
          },
          headerTintColor: '#fff',
        }}
      />
      <Stack.Screen
        name="VolDetail"
        component={VolDetail}
        options={{
          headerStyle: {
            backgroundColor: '#8ea9df',
          },
          headerTintColor: '#fff',
        }}
      />
    </Stack.Navigator>
  );
}
