import React, {Component} from 'react';
import {createDrawerNavigator} from '@react-navigation/drawer';
import {NavigationContainer} from '@react-navigation/native';
import {
  AcierStack,
  CombatStack,
  DragonStack,
  EauStack,
  ElectrikStack,
  FeeStack,
  FeuStack,
  GlaceStack,
  InsecteStack,
  NormalStack,
  PlanteStack,
  PoisonStack,
  PsyStack,
  RocheStack,
  SolStack,
  SpectreStack,
  TenebreStack,
  VolStack,
} from './Stacks';
import {Image} from 'react-native';

const Drawer = createDrawerNavigator();

export function Navigation() {
  return (
    <NavigationContainer>
      <Drawer.Navigator initialRouteName="Home">
        <Drawer.Screen
          name="Acier"
          component={AcierStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/acier.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Combat"
          component={CombatStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/combat.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Dragon"
          component={DragonStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/dragon.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Eau"
          component={EauStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/eau.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Electrik"
          component={ElectrikStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/electrik.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Fée"
          component={FeeStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/fee.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Feu"
          component={FeuStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/feu.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Glace"
          component={GlaceStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/glace.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Insecte"
          component={InsecteStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/insecte.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Normal"
          component={NormalStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/normal.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Plante"
          component={PlanteStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/plante.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Poison"
          component={PoisonStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/poison.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Psy"
          component={PsyStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/psy.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Roche"
          component={RocheStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/roche.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Sol"
          component={SolStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/sol.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Spectre"
          component={SpectreStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/spectre.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Ténèbre"
          component={TenebreStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/tenebre.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
        <Drawer.Screen
          name="Vol"
          component={VolStack}
          options={{
            drawerIcon: () => {
              return (
                <Image
                  source={require('../assets/vol.png')}
                  style={{height: 24, width: 24}}
                />
              );
            },
          }}
        />
      </Drawer.Navigator>
    </NavigationContainer>
  );
}
