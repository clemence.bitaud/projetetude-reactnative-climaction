## Projet
Projet personnel en cours de réalisation en parallèle de mon alternance.

Réalisation d'une application mobile en React Native affichant la liste des pokemons par type récupérés de la PokeApi. Dans un second temps, on pourra effectuer une rechercher d'un pokemon par son nom et dans un troisième temps pouvoir ajouter un pokemon à sa liste de favoris et pouvoir les consulter.
