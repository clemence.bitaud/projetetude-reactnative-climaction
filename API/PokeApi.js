export function getPokemonByType(type) {
  const url = 'https://pokeapi.co/api/v2/type/' + type;
  return fetch(url)
    .then(response => response.json())
    .catch(error => console.log(error));
}

export function getPokemonByName(name) {
  const url = 'https://pokeapi.co/api/v2/pokemon/' + name;
  return fetch(url)
    .then(response => response.json())
    .catch(error => console.log(error));
}

export function getPokemonSpeciesByName(name) {
  const url = 'https://pokeapi.co/api/v2/pokemon-species/' + name;
  return fetch(url)
    .then(response => response.json())
    .catch(error => console.log(error));
}
